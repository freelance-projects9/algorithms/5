public class VehicleHiringManager {

    private TreeLocatorMap tlm;

    public VehicleHiringManager() {
        tlm = new TreeLocatorMap();
    }

    // Returns the locator map.
    public LocatorMap<String> getLocatorMap() {
        return tlm;
    }

    // Sets the locator map.
    public void setLocatorMap(LocatorMap<String> locatorMap) {
        tlm = (TreeLocatorMap) locatorMap;
    }

    // Inserts the vehicle id at location loc if it does not exist and returns true.
    // If id already exists, the method returns false.
    public boolean addVehicle(String id, Location loc) {
        boolean add = (boolean) tlm.add(id, loc).first;
        return add;
    }

    // Moves the vehicle id to location loc if id exists and returns true. If id not
    // exist, the method returns false.
    public boolean moveVehicle(String id, Location loc) {
        boolean move = (boolean) tlm.move(id, loc).first;
        return move;
    }

    // Removes the vehicle id if it exists and returns true. If id does not exist,
    // the method returns false.
    public boolean removeVehicle(String id) {
        boolean remove = (boolean) tlm.remove(id).first;
        return remove;
    }

    // Returns the location of vehicle id if it exists, null otherwise.
    public Location getVehicleLoc(String id) {
        return (Location) tlm.getLoc(id).first;
    }

    // Returns all vehicles located within a square of side 2*r centered at loc
    // (inclusive of the boundaries).
    public List<String> getVehiclesInRange(Location loc, int r) {
        Location loc1 = new Location(loc.x - r, loc.y - r);
        Location loc2 = new Location(loc.x + r, loc.y + r);
        Pair p = tlm.getInRange(loc1, loc2);
        List<String> list = (List<String>) p.first;
        return list;
    }

}
