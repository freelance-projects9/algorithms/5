 class TreeLocatorNode<T> {
    public List<T> data = new LinkedList<>();
    public Location location;
    public TreeLocatorNode<T> c1;
    public TreeLocatorNode<T> c2;
    public TreeLocatorNode<T> c3;
    public TreeLocatorNode<T> c4;

    TreeLocatorNode(T e, Location location) {
        this.location = location;
        this.data.insert(e);
    }

}