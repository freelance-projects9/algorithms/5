public class LinkedList<T> implements List<T> {

    private class Node<T> {

        public T data;
        public Node<T> next;

        public Node(T val) {
            data = val;
            next = null;
        }
    }

    private Node<T> head;
    private Node<T> current;

    LinkedList() {
        head = current = null;
    }

    @Override
    public boolean empty() {
        return head == null;
    }

    @Override
    public boolean full() {
        return false;
    }

    @Override
    public void findFirst() {
        current = head;
    }

    @Override
    public void findNext() {
        current = current.next;
    }

    @Override
    public boolean last() {
        return current.next == null;
    }

    @Override
    public T retrieve() {
        if (current == null) {
            return null;
        } else {
            return current.data;
        }
    }

    @Override
    public void update(T e) {
        current.data = e;
    }

    @Override
    public void insert(T e) {
        if (head == null) {
            current = head = new Node<>(e);
            return;
        }
        Node<T> tmp = new Node<>(e);
        if (!last()) {
            Node<T> temp = current;
            while (true) {
                if (temp.next == null)
                    break;
                temp = temp.next;
            }
            temp.next = tmp;
            current = tmp;
        } else {
            current.next = tmp;
            current = tmp;
        }
        return;

    }

    @Override
    public void remove() {
        if (head != null) {
            if (current != head) {
                Node<T> tmp = head;
                while (true) {
                    if (tmp.next == current)
                        break;
                    tmp = tmp.next;
                }
                tmp.next = current.next;
            } else {
                head = head.next;
            }
            if (current.next == null) {
                current = head;
            } else {
                current = current.next;
            }
        }
    }


}


