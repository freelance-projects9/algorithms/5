public class BST<K extends Comparable<K>, T> implements Map<K, T> {

    private class BSTNode<K, T> {

        public K key;
        public T data;
        public BSTNode<K, T> left, right;

        BSTNode(K key, T data) {
            this.key = key;
            this.data = data;
            left = right = null;
        }
    }

    private BSTNode<K, T> root, current;

    BST() {
        root = current = null;
    }

    @Override
    public boolean empty() {
        return root == null;
    }

    @Override
    public boolean full() {
        return false;
    }

    @Override
    public T retrieve() {
        return (T) current.data;
    }

    @Override
    public void update(T e) {
        current.data = e;
    }

    @Override
    public Pair<Boolean, Integer> find(K key) {
        BSTNode<K, T> p = root, q = root;
        int count = 0;
        if (p != null) {
            while (true) {
                if (p == null) {
                    break;
                }
                count += 1;
                q = p;
                if (key.compareTo(p.key) > 0) {
                    p = p.right;
                } else if (key.compareTo(p.key) < 0) {
                    p = p.left;
                } else {
                    current = p;
                    return new Pair(true, count);
                }
            }
            current = q;
        }
        return new Pair(false, count);
    }

    @Override
    public Pair<Boolean, Integer> insert(K key, T data) {
        BSTNode<K, T> p = root, q = root;
        int count = 0;
        boolean found = false;
        if (p != null) {
            while (true) {
                if (p == null) {
                    break;
                }
                count += 1;
                q = p;
                int r = key.compareTo(p.key);
                if (r == 0) {
                    current = p;
                    found = true;
                    break;
                } else if (r > 0) {
                    p = p.right;
                } else {
                    p = p.left;
                }
            }
            current = q;
        }
        if (found) {
            return new Pair(!found, count);
        }
        BSTNode<K, T> tmp = new BSTNode<>(key, data);
        if (!empty()) {
            int s = key.compareTo(current.key);
            if (s < 0) {
                current.left = tmp;
            } else {
                current.right = tmp;
            }
            current = tmp;
        } else {
            root = current = tmp;
        }
        return new Pair(!found, count);
    }

    private boolean isleaf(BSTNode<K, T> p) {
        return (p.left != null) && (p.right != null);
    }

    private boolean left(BSTNode<K, T> p) {
        return p.left != null;
    }

    @Override
    public Pair<Boolean, Integer> remove(K key) {
        K k1 = key;
        BSTNode<K, T> p = root, q = null; // Parent of p
        int count = 0;
        while (p != null) {
            count++;
            int r =k1.compareTo(p.key);
            if (r > 0) {
                q = p;
                p = p.right;
            } else if (r < 0) {
                q = p;
                p = p.left;
            } else {
                // Found the key
                // Check the three cases
                if (isleaf(p)) {
                    // Case 3: two children
                    // Search for the min in the right subtree
                    BSTNode<K, T> min = p.right;
                    q = p;
                    while (min.left != null) {
                        q = min;
                        min = min.left;
                    }
                    p.key = min.key;
                    p.data = min.data;
                    k1 = min.key;
                    p = min;
                    // Now fall back to either case 1 or 2
                }
                // The subtree rooted at p will change here
                if (!left(p)) { // One child
                    p = p.right;
                } else { // One or no children
                    p = p.left;
                }
                if (q == null) {
                    root = p;
                } else {
                    if (k1.compareTo(q.key) > 0) {
                        q.right = p;
                    } else {
                        q.left = p;
                    }
                }
                current = root;
                return new Pair(true, count);
            }
        }
        return new Pair(false, count); // Not found
    }

    @Override
    public List<K> getAll() {
        List<K> keys = new LinkedList<>();
        BSTNode<K, T> p = root;
        recursive(keys, p);
        return keys;
    }

    private void recursive(List<K> keys, BSTNode<K, T> p) {
        if (p != null) {
            recursive(keys, p.left);
            keys.insert(p.key);
            recursive(keys, p.right);
        }
    }

}
