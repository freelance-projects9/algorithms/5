public class TreeLocatorMap<K extends Comparable<K>> implements LocatorMap<K> {

    private BST bst;
    private TreeLocator tl;

    TreeLocatorMap() {
        this.bst = new BST();
        this.tl = new TreeLocator();
    }

    @Override
    public Map<K, Location> getMap() {
        return this.bst;
    }

    @Override
    public Locator<K> getLocator() {
        return this.tl;
    }

    @Override
    public Pair<Boolean, Integer> add(K k, Location loc) {
        Pair<Boolean, Integer> pair = bst.find(k);
        boolean p = pair.first;
        if (p == false) {
            bst.insert(k, loc);
            int count = tl.add(k, loc);
            return new Pair<>(true, count);
        }
        return new Pair<>(!p, pair.second);
    }

    @Override
    public Pair<Boolean, Integer> move(K k, Location loc) {
        Pair<Boolean, Integer> pair = bst.find(k);
        boolean p = pair.first;
        if (p == true) {
            tl.remove(k, (Location) bst.retrieve());
            tl.add(k, loc);
            bst.update(loc);
            return new Pair<>(true, pair.second);
        }
        return new Pair<>(false, pair.second);
    }

    @Override
    public Pair<Location, Integer> getLoc(K k) {
        Pair<Boolean, Integer> pair = bst.find(k);
        boolean p = pair.first;
        if (p == false) {
            return new Pair<>(null, pair.second);
        }
        Location loc = (Location) bst.retrieve();
        return new Pair<>(loc, pair.second);
    }

    @Override
    public Pair<Boolean, Integer> remove(K k) {
        Pair<Boolean, Integer> pair = bst.find(k);
        boolean p = pair.first;
        if (p) {

            Location loc = (Location) bst.retrieve();
            bst.remove(k);
            tl.remove(k, loc);
            return new Pair<>(true, pair.second);
        }
        return new Pair<>(false, pair.second);

    }

    @Override
    public List<K> getAll() {
        return bst.getAll();
    }

    @Override
    public Pair<List<K>, Integer> getInRange(Location lowerLeft, Location upperRight) {
        Pair<List<Pair<Location, List>>, Integer> pair = tl.inRange(lowerLeft, upperRight);
        List<Pair<Location, List>> l = pair.first;
        int t = pair.second;
        if (l.empty()) {
            return new Pair(null, t);
        }
        List<K> ll = fill(l);
        return new Pair(ll, t);
    }

    private List<K> fill(List<Pair<Location, List>> l) {
        List<K> ll = new LinkedList();
        l.findFirst();
        while (true) {
            if (l.last())
                break;
            Pair<Location, List> pp = l.retrieve();
            List key = pp.second;
            fillHelper(key, ll);
            l.findNext();
        }
        List<K> key = l.retrieve().second;
        fillHelper(key, ll);
        return ll;
    }

    private void fillHelper(List key, List ll) {
        key.findFirst();
        while (!key.last()) {
            ll.insert((K) key.retrieve());
            key.findNext();
        }
        ll.insert(key.retrieve());
    }
}

