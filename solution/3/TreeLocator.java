public class TreeLocator<T> implements Locator {

    private TreeLocatorNode<T> root, current;

    TreeLocator() {
        root = current = null;
    }

    @Override
    public int add(Object e, Location loc) {
        TreeLocatorNode<T> p, q = current;
        Pair<Boolean, Integer> pair = find(loc);
        boolean found = pair.first;
        int count = pair.second;
        if (found) {
            current.data.insert((T) e);
            current = q;
            return count;
        } else {
            p = new TreeLocatorNode<>((T) e, loc);
            if (root == null) {
                root = current = p;
                return count;
            }
            if (checkchild1(current.location, loc)) {
                current.c1 = p;
            } else if (checkchild2(current.location, loc)) {
                current.c2 = p;
            } else if (checkchild3(current.location, loc)) {
                current.c3 = p;
            } else if (checkchild4(current.location, loc)) {
                current.c4 = p;
            }
            current = p;
            return count;
        }
    }

    private Pair<Boolean, Integer> find(Location loc) {
        TreeLocatorNode<T> p = root, q = root;
        int count = 0;
        if (p != null) {
            while (true) {
                if (p == null) {
                    break;
                }
                count++;
                q = p;
                if (p.location.x == loc.x && p.location.y == loc.y) {
                    current = p;
                    return new Pair(true, count);
                } else if (checkchild1(p.location, loc)) {
                    p = p.c1;
                } else if (checkchild2(p.location, loc)) {
                    p = p.c2;
                } else if (checkchild3(p.location, loc)) {
                    p = p.c3;
                } else if (checkchild4(p.location, loc)) {
                    p = p.c4;
                }
            }
            current = q;
            return new Pair(false, count);
        } else {
            return new Pair(false, count);
        }
    }

    private boolean checkchild4(Location l1, Location l2) {
        return l2.x >= l1.x && l2.y < l1.y;
    }

    private boolean checkchild3(Location l1, Location l2) {
        return l2.x > l1.x && l2.y >= l1.y;
    }

    private boolean checkchild2(Location l1, Location l2) {
        return l2.x <= l1.x && l2.y > l1.y;
    }

    private boolean checkchild1(Location l1, Location l2) {
        return l2.x < l1.x && l2.y <= l1.y;
    }


    @Override
    public Pair<List<T>, Integer> get(Location loc) {
        TreeLocatorNode<T> q = root;
        Pair<Boolean, Integer> pair = find(loc);
        boolean p = pair.first;
        if (p) {
            List<T> list = (List<T>) current.data;
            current = q;
            return new Pair(list, pair.second);
        }
        current = q;
        return new Pair(null, pair.second);
    }

    @Override
    public Pair<Boolean, Integer> remove(Object e, Location loc) {
        TreeLocatorNode<T> q = root;
        Pair<Boolean, Integer> pair = find(loc);
        if (!pair.first) {
            current = q;
            return new Pair(false, pair.second);
        }
        if (contains(e)) {
            current.data.remove();
            current = q;
            return new Pair(true, pair.second);
        } else {
            current = q;
            return new Pair(false, pair.second);
        }

    }

    private boolean contains(Object e) {
        boolean empty = current.data.empty();
        if (!empty) {
            current.data.findFirst();
            while (true) {
                if (current.data.last()) {
                    break;
                }
                if (!current.data.retrieve().equals(e)) {
                    current.data.findNext();
                } else {
                    return true;
                }
            }
            if (current.data.retrieve().equals(e)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Pair<Location, List>> getAll() {
        TreeLocatorNode<T> p = root;
        List<Pair<Location, List>> list = new LinkedList<>();
        recursive(p, list);
        return list;
    }

    private void recursive(TreeLocatorNode<T> p, List<Pair<Location, List>> keys) {
        if (p != null)
            return;
        recursive(p.c1, keys);
        recursive(p.c2, keys);
        recursive(p.c3, keys);
        recursive(p.c4, keys);
        Pair<Location, List> pair = new Pair(p.location, p.data);
        keys.insert(pair);
    }

    @Override
    public Pair<List<Pair<Location, List>>, Integer> inRange(Location lowerLeft, Location upperRight) {
        TreeLocatorNode<T> p = root;
        List<Pair<Location, List>> list = new LinkedList<>();
        return new Pair<>(list, getInRange(0, list, p, lowerLeft, upperRight));
    }

    private int getInRange(int count, List<Pair<Location, List>> list, TreeLocatorNode node, Location
            lowerLeft, Location upperRight) {
        if (node == null) {
            return count;
        }
        count++;
        Location loc = node.location;
        boolean lower = (lowerLeft.x <= loc.x && lowerLeft.y <= loc.y);
        boolean upper = (upperRight.x >= loc.x && upperRight.y >= loc.y);
        if (lower && upper) {
            list.insert(new Pair(node.location, node.data));
        }
        if (checkChild1(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c1, lowerLeft, upperRight);
        }
        if (checkChild2(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c2, lowerLeft, upperRight);
        }
        if (checkChild3(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c3, lowerLeft, upperRight);
        }
        if (checkChild4(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c4, lowerLeft, upperRight);
        }
        return count;

    }

    private boolean checkChild4(Location lowerLeft, Location upperRight, Location current) {
        return upperRight.x >= current.x && lowerLeft.y < current.y;
    }

    private boolean checkChild3(Location lowerLeft, Location upperRight, Location current) {
        return upperRight.x > current.x && upperRight.y >= current.y;
    }

    private boolean checkChild2(Location lowerLeft, Location upperRight, Location current) {
        return lowerLeft.x <= current.x && upperRight.y > current.y;
    }

    private boolean checkChild1(Location lowerLeft, Location upperRight, Location current) {
        return lowerLeft.x < current.x && lowerLeft.y <= current.y;
    }


}
