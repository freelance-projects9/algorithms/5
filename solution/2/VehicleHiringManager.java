public class VehicleHiringManager {

    private TreeLocatorMap treeLocatorMap;

    public VehicleHiringManager() {
        treeLocatorMap = new TreeLocatorMap();
    }

    // Returns the locator map.
    public LocatorMap<String> getLocatorMap() {
        return this.treeLocatorMap;
    }

    // Sets the locator map.
    public void setLocatorMap(LocatorMap<String> locatorMap) {
        this.treeLocatorMap = (TreeLocatorMap) locatorMap;
    }

    // Inserts the vehicle id at location loc if it does not exist and returns true.
    // If id already exists, the method returns false.
    public boolean addVehicle(String id, Location loc) {
        Pair t = treeLocatorMap.add(id, loc);
        return (boolean) t.first;
    }

    // Moves the vehicle id to location loc if id exists and returns true. If id not
    // exist, the method returns false.
    public boolean moveVehicle(String id, Location loc) {
        Pair t = treeLocatorMap.move(id, loc);
        return (Boolean) t.first;

    }

    // Removes the vehicle id if it exists and returns true. If id does not exist,
    // the method returns false.
    public boolean removeVehicle(String id) {
        Pair t = treeLocatorMap.remove(id);
        return (Boolean) t.first;
    }

    // Returns the location of vehicle id if it exists, null otherwise.
    public Location getVehicleLoc(String id) {
        Pair t = treeLocatorMap.getLoc(id);
        return (Location) t.first;
    }

    // Returns all vehicles located within a square of side 2*r centered at loc
    // (inclusive of the boundaries).
    public List<String> getVehiclesInRange(Location loc, int r) {
        Location lowerLeft = new Location(loc.x - r, loc.y - r);
        Location upperRight = new Location(loc.x + r, loc.y + r);
        Pair t = treeLocatorMap.getInRange(lowerLeft, upperRight);
        return (List<String>) t.first;

    }

    public void printBST() {
        treeLocatorMap.printBST();
    }
}
