
public class TreeLocatorMap<K extends Comparable<K>> implements LocatorMap<K> {

    private BST bst;
    private TreeLocator treeLocator;

    public TreeLocatorMap() {
        this.bst = new BST();
        this.treeLocator = new TreeLocator();
    }

    @Override
    public Map<K, Location> getMap() {
        return this.bst;
    }

    @Override
    public Locator<K> getLocator() {
        return this.treeLocator;
    }

    @Override
    public Pair<Boolean, Integer> add(K k, Location loc) {
        Pair<Boolean, Integer> pair = bst.find(k);
        if (pair.first) {
            Pair<Boolean, Integer> t = new Pair<Boolean, Integer>(false, pair.second);
            return t;
        } else {
            bst.insert(k, loc);
            int count = treeLocator.add(k, loc);
            return new Pair<Boolean, Integer>(true, count);
        }
    }

    @Override
    public Pair<Boolean, Integer> move(K k, Location loc) {
        Pair<Boolean, Integer> pair = bst.find(k);
        if (!pair.first) {
            return new Pair<>(false, pair.second);
        } else {
            Location loc2 = (Location) bst.retrieve();
            treeLocator.remove(k, loc2);
            treeLocator.add(k, loc);
            bst.update(loc);
            return new Pair<>(true, pair.second);
        }
    }

    @Override
    public Pair<Location, Integer> getLoc(K k) {
        Pair<Boolean, Integer> pair = bst.find(k);
        if (!pair.first) {
            Pair t = new Pair<>(null, pair.second);
            return t;
        } else {
            Location loc2 = (Location) bst.retrieve();
            Pair t = new Pair<>(loc2, pair.second);
            return t;

        }
    }

    @Override
    public Pair<Boolean, Integer> remove(K k) {
        Pair<Boolean, Integer> pair = bst.find(k);
        if (!pair.first) {
            return new Pair<>(false, pair.second);
        } else {
            Location loc2 = (Location) bst.retrieve();
            treeLocator.remove(k, loc2);
            bst.remove(k);
            return new Pair<>(true, pair.second);

        }
    }

    @Override
    public List<K> getAll() {
        return bst.getAll();
    }

    @Override
    public Pair<List<K>, Integer> getInRange(Location lowerLeft, Location upperRight) {
        Pair<List<Pair<Location, List>>, Integer> pair = treeLocator.inRange(lowerLeft, upperRight);
        List<Pair<Location, List>> l = pair.first;
        List<K> ll = new LinkedList();
        l.findFirst();
        while (!l.last()) {
            Pair<Location, List> pp = l.retrieve();
            List key = pp.second;
            key.findFirst();
            while (true) {
                if (key.last())
                    break;
                ll.insert((K) key.retrieve());
                key.findNext();
            }
            ll.insert((K) key.retrieve());
            l.findNext();
        }
        List<K> key = l.retrieve().second;
        key.findFirst();
        while (true) {
            if(key.last())
                break;
            ll.insert(key.retrieve());
            key.findNext();
        }
        ll.insert(key.retrieve());
        return new Pair(ll, pair.second);
    }


    public void printBST() {
        bst.printBFS();
    }
}
