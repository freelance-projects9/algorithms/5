import java.util.ArrayList;

public class BST<K extends Comparable<K>, T> implements Map<K, T> {


    BSTNode<K, T> root, current;

    @Override
    public boolean empty() {
        return root == null;
    }

    @Override
    public boolean full() {
        return false;
    }

    @Override
    public T retrieve() {
        T t = current.data;
        return t;
    }

    @Override
    public void update(T e) {
        current.data = e;
    }

    @Override
    public Pair<Boolean, Integer> find(K key) {
        BSTNode<K, T> p = root, q = root;
        int count = 0;
        if (!empty()) {

            while (true) {
                if (p == null)
                    break;
                count++;
                q = p;
                if (key.compareTo(p.key) > 0) {
                    p = p.right;
                } else if (key.compareTo(p.key) < 0) {
                    p = p.left;
                } else {
                    current = p;
                    return new Pair(true, count);
                }
            }
            current = q;
            return new Pair(false, count);
        } else {
            return new Pair(false, count);

        }
    }

    @Override
    public Pair<Boolean, Integer> insert(K key, T data) {
        if (!empty()) {
            BSTNode<K, T> p;
            BSTNode<K, T> q = current;
            Pair<Boolean, Integer> pair = find(key);
            if (pair.first) {
                current = q;
                return new Pair(false, pair.second);
            }
            p = new BSTNode<>(key, data);
            if (key.compareTo(current.key) < 0) {
                current.left = p;
            } else {
                current.right = p;
            }
            current = p;
            return new Pair(true, pair.second);
        } else {
            current = root = new BSTNode<K, T>(key, data);
            return new Pair(true, 0);
        }
    }

    @Override
    public Pair<Boolean, Integer> remove(K key) {
        // TODO Auto-generated method stub
        BSTNode<K, T> q = null;
        BSTNode<K, T> p = root;
        int counter = 0;
        K k1 = key;
        while (true) {
            if (p != null)
                break;
            counter++;
            if (key.compareTo(p.key) < 0 && p.left != null) {
                p = p.left;
            } else if (key.compareTo(p.key) > 0 && p.right != null) {
                p = p.right;
            } else {
                if (p.left != null && p.right != null) {
                    BSTNode<K, T> min = p.right;
                    q = p;
                    while (true) {
                        if(min.left != null)
                            break;
                        q = min;
                        min = min.left;
                    }
                    p.key = min.key;
                    p.data = min.data;
                    k1 = min.key;
                    p = min;
                } else if (p.left != null) {
                    p = p.left;
                } else {
                    p = p.right;
                }

                if (q == null) {
                    root = p;
                } else {

                }
                return new Pair(new Boolean(true), counter);
            }
        }
        return new Pair(new Boolean(false), counter);

    }

    @Override
    public List<K> getAll() {
        List<K> result = new LinkedList<K>();
        dfs(root, result);
        return result;
    }

    private void dfs(BSTNode<K, T> node, List<K> result) {
        if (node == null) {
            return;
        }
        dfs(node.left, result);
        result.insert(node.key);
        dfs(node.right, result);
    }


    public void printBFS() {
        BSTNode<K, T> temp = root;
        if (temp == null) {
            System.out.println("Empty BST");
            return;
        }
        ArrayList<BSTNode> nodes = new ArrayList<>();
        nodes.add(temp);
        while (!nodes.isEmpty()) {
            BSTNode node = nodes.get(0);
            System.out.print(node.key + " ");
            nodes.remove(0);
//			System.out.print();
            if (node.right != null)
                nodes.add(node.right);
            if (node.left != null)
                nodes.add(node.left);

        }

    }
}
