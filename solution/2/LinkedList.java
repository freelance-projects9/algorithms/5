
public class LinkedList<T> implements List<T> {
	private Node<T> head;
	private Node<T> current;
	private int size;


	public LinkedList() {
		head = current = null;
		size = 0;
	}

	@Override
	public boolean empty() {
		return head == null;
	}

	@Override
	public boolean last() {
		return current.next == null;
	}

	@Override
	public boolean full() {
		return false;
	}

//    public int size() {
//        return size;
//    }

	public void findFirst() {
		current = head;
	}

	@Override
	public void findNext() {
		current = current.next;
	}

	@Override
	public T retrieve() {
		if (current == null) return null;
		return current.data;
	}

	@Override
	public void update(T val) {
		current.data = val;
	}

	@Override
	public void insert(T val) {
		Node<T> tmp = new Node<>(val);
		if (!empty()) {
			Node<T> temp = current;
			while (true) {
				if (temp.next != null) {
					temp = temp.next;
				} else {
					break;
				}
			}
			temp.next = tmp;
			current = tmp;
		} else {
			current = head = new Node<T>(val);
		}
		size++;
	}

	@Override
	public void remove() {
		if (!empty()) {
			if (current != head) {
				Node<T> tmp = head;
				while (true) {
					if (tmp.next == current) break;
					tmp = tmp.next;
				}
				tmp.next = current.next;
			} else {
				head = head.next;
			}
			if (current.next != null) {
				current = current.next;
			} else {
				current = head;
			}
			size--;
		} else {
			return;
		}
	}

	public boolean contains(T data) {
		Node temp = head;
		while (temp != null) {
			if (temp.data == data)
				return true;
			temp = temp.next;
		}
		return false;
	}

	public void display() {
		if (head == null) {
			System.out.println("empty list");
			return;
		}
		Node temp = head;
		while (temp != null) {
			System.out.print(temp.data + ",");
			temp = temp.next;
		}
		System.out.println();

	}

}
