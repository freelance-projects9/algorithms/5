public class TreeLocatorNode<T> {
    public List<T> data = new LinkedList<>();
    public Location location;
    public TreeLocatorNode<T> c1, c2, c3, c4;

    TreeLocatorNode(T e, Location location) {
        this.location = location;
        this.data.insert(e);
    }
}