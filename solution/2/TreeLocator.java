
public class TreeLocator<T> implements Locator<T> {

    private TreeLocatorNode<T> root, current;

    TreeLocator() {
        root = current = null;
    }

    @Override
    public int add(T e, Location loc) {
        TreeLocatorNode<T> p, q = current;
        Pair<Boolean, Integer> pair = find(loc);
        if (pair.first) {
            current.data.insert((T) e);  // findkey() modified current
            current = q;
            return pair.second; // key already in the BST
        } else {
            p = new TreeLocatorNode<T>((T) e, loc);
            if (root != null) {
                // current is pointing to parent of the new key
                if (compare(current.location, loc) == 1) {
                    current.c1 = p;
                } else if (compare(current.location, loc) == 2) {
                    current.c2 = p;
                } else if (compare(current.location, loc) == 3) {
                    current.c3 = p;
                } else {
                    current.c4 = p;
                }
                current = p;
                return pair.second;
            } else {
                root = current = p;
                return pair.second;
            }

        }
    }

    private Pair<Boolean, Integer> find(Location loc) {
        TreeLocatorNode<T> p = root, q = root;
        int count = 0;
        if (root == null)
            return new Pair(false, count);
        while (true) {
            if (p == null) {
                break;
            }
            count++;
            q = p;
            if (compare(p.location, loc) == 0) {
                current = p;
                return new Pair(true, count);
            } else if (compare(p.location, loc) == 1) {
                p = p.c1;
            } else if (compare(p.location, loc) == 2) {
                p = p.c2;
            } else if (compare(p.location, loc) == 3) {
                p = p.c3;
            } else {
                p = p.c4;
            }
        }
        current = q;
        return new Pair(false, count);
    }

    private int compare(Location l1, Location l2) {
        if (isEqual(l1,l2)) {
            return 0;
        } else if (isChild2(l1,l2)) {
            return 2;
        } else if (isChild3(l1,l2)) {
            return 3;
        } else if (isChild4(l1,l2)) {
            return 4;
        } else {
            return 1;
        }
    }

    private boolean isChild4(Location l1, Location l2) {
        return l2.x >= l1.x && l2.y < l1.y;
    }

    private boolean isChild3(Location l1, Location l2) {
        return l2.x > l1.x && l2.y >= l1.y;
    }

    private boolean isChild2(Location l1, Location l2) {
        return l2.x <= l1.x && l2.y > l1.y;
    }

    private boolean isEqual(Location l1, Location l2) {
        return l2.x == l1.x && l2.y == l1.y;
    }

    @Override
    public Pair<List<T>, Integer> get(Location loc) {
        TreeLocatorNode<T> q = root;
        Pair<Boolean, Integer> pair = find(loc);
        if (!pair.first) {
            current = q;
            return new Pair(null, pair.second);
        } else {
            List<T> list = (List<T>) current.data;
            current = q;
            return new Pair(list, pair.second);

        }

    }

    @Override
    public Pair<Boolean, Integer> remove(T e, Location loc) {
        TreeLocatorNode<T> q = root;
        Pair<Boolean, Integer> pair = find(loc);
        if (!pair.first) {
            current = q;
            return new Pair(false, pair.second);
        } else {
            if (contains((T) e)) {
                current.data.remove();
                current = q;
                return new Pair(true, pair.second);
            } else {
                current = q;
                return new Pair(false, pair.second);
            }
        }
    }

    private boolean contains(Object e) {
        if (current.data.empty())
            return false;
        current.data.findFirst();
        while (!current.data.last()) {
            if (current.data.retrieve().equals(e))
                return true;
            current.data.findNext();
        }
        if (current.data.retrieve().equals(e))
            return true;
        return false;
    }

    @Override
    public List<Pair<Location, List<T>>> getAll() {
        TreeLocatorNode<T> p = root;
        List<Pair<Location, List<T>>> list = new LinkedList<>();
        recursive(p, list);
        return list;
    }

    private void recursive(TreeLocatorNode<T> p, List<Pair<Location, List<T>>> keys) {
        if (p == null) {
            return;
        } else {
            recursive(p.c1, keys);
            recursive(p.c2, keys);
            recursive(p.c3, keys);
            recursive(p.c4, keys);
            keys.insert(new Pair(p.location, p.data));
        }
    }

    @Override
    public Pair<List<Pair<Location, List<T>>>, Integer> inRange(Location lowerLeft, Location upperRight) {
        TreeLocatorNode<T> p = root;
        List<Pair<Location, List<T>>> list = new LinkedList<>();
        int count = getInRange(0, list, p, lowerLeft, upperRight);
        return new Pair<List<Pair<Location, List<T>>>, Integer>(list, count);
    }

    private int getInRange(int count, List<Pair<Location, List<T>>> list, TreeLocatorNode node, Location lowerLeft, Location upperRight) {
        if (node == null) {
            return count;
        }
        count++;
        if (isvalid(lowerLeft, upperRight, node.location)) {
            list.insert(new Pair(node.location, node.data));
        }
        if (isvalidCh1(lowerLeft, node.location)) {
            count += getInRange(0, list, node.c1, lowerLeft, upperRight);
        }
        if (isvalidCh2(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c2, lowerLeft, upperRight);
        }
        if (isvalidCh3(upperRight, node.location)) {
            count += getInRange(0, list, node.c3, lowerLeft, upperRight);
        }
        if (isvalidCh4(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c4, lowerLeft, upperRight);
        }
        return count;
    }


    private boolean isvalid(Location lowerLeft, Location upperRight, Location current) {
        return (lowerLeft.x <= current.x && lowerLeft.y <= current.y) && (upperRight.x >= current.x && upperRight.y >= current.y);
    }

    private boolean isvalidCh1(Location lowerLeft, Location current) {
        return lowerLeft.x < current.x && lowerLeft.y <= current.y;
    }

    private boolean isvalidCh2(Location lowerLeft, Location upperRight, Location current) {
        return lowerLeft.x <= current.x && upperRight.y > current.y;
    }

    private boolean isvalidCh3(Location upperRight, Location current) {
        return upperRight.x > current.x && upperRight.y >= current.y;
    }

    private boolean isvalidCh4(Location lowerLeft, Location upperRight, Location current) {
        return upperRight.x >= current.x && lowerLeft.y < current.y;
    }
}
