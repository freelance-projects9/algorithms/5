public class VehicleHiringManager {

    private TreeLocatorMap treeLocatorMap;

    public VehicleHiringManager() {
        treeLocatorMap = new TreeLocatorMap();
    }

    // Returns the locator map.
    public LocatorMap<String> getLocatorMap() {
        return this.treeLocatorMap;
    }

    // Sets the locator map.
    public void setLocatorMap(LocatorMap<String> locatorMap) {
        this.treeLocatorMap = (TreeLocatorMap) locatorMap;
    }

    // Inserts the vehicle id at location loc if it does not exist and returns true.
    // If id already exists, the method returns false.
    public boolean addVehicle(String id, Location loc) {
        return (Boolean) treeLocatorMap.add(id, loc).first;
    }

    // Moves the vehicle id to location loc if id exists and returns true. If id not
    // exist, the method returns false.
    public boolean moveVehicle(String id, Location loc) {
        return (Boolean) treeLocatorMap.move(id, loc).first;

    }

    // Removes the vehicle id if it exists and returns true. If id does not exist,
    // the method returns false.
    public boolean removeVehicle(String id) {

        return (Boolean) treeLocatorMap.remove(id).first;
    }

    // Returns the location of vehicle id if it exists, null otherwise.
    public Location getVehicleLoc(String id) {
        return (Location) treeLocatorMap.getLoc(id).first;
    }

    // Returns all vehicles located within a square of side 2*r centered at loc
    // (inclusive of the boundaries).
    public List<String> getVehiclesInRange(Location loc, int r) {
        Location lowerLeft = new Location(loc.x - r, loc.y - r);
        Location upperRight = new Location(loc.x + r, loc.y + r);
        return (List<String>) treeLocatorMap.getInRange(lowerLeft, upperRight).first;
    }

    public void printBST() {
        treeLocatorMap.printBST();
    }
}
