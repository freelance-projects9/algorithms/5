
public class TreeLocator<T> implements Locator<T> {

    private TreeLocatorNode<T> root, current;

    TreeLocator() {
        root = current = null;
    }

    @Override
    public int add(T e, Location loc) {
        TreeLocatorNode<T> p, q = current;
        Pair<Boolean, Integer> pair = find(loc);
        if (!pair.first) {
            p = new TreeLocatorNode<T>((T) e, loc);
            if (root != null) {

                // current is pointing to parent of the new key
                int pos = compare(current.location, loc);
                if (pos == 1) {
                    current.c1 = p;
                } else if (pos == 2) {
                    current.c2 = p;
                } else if (pos == 3) {
                    current.c3 = p;
                } else if (pos == 4) {
                    current.c4 = p;
                }
                current = p;
                return pair.second;
            } else {
                root = current = p;
                return pair.second;
            }
        } else {
            current.data.insert((T) e);  // findkey() modified current
            current = q;
            return pair.second; // key already in the BST
        }
    }

    private Pair<Boolean, Integer> find(Location loc) {
        TreeLocatorNode<T> p = root, q = root;
        int count = 0;
        if (root == null)
            return new Pair(false, count);
        while (p != null) {
            count++;
            q = p;
            int pos = compare(p.location, loc);
            if (pos == 0) {
                current = p;
                return new Pair(true, count);
            } else if (pos == 1) {
                p = p.c1;
            } else if (pos == 2) {
                p = p.c2;
            } else if (pos == 3) {
                p = p.c3;
            } else if (pos == 4) {
                p = p.c4;
            }
        }
        current = q;
        return new Pair(false, count);
    }

    private int compare(Location l1, Location l2) {
        if (l2.x < l1.x && l2.y <= l1.y) {
            return 1;
        } else if (l2.x <= l1.x && l2.y > l1.y) {
            return 2;
        } else if (l2.x > l1.x && l2.y >= l1.y) {
            return 3;
        } else if (l2.x >= l1.x && l2.y < l1.y) {
            return 4;
        } else {
            return 0;
        }
    }

    @Override
    public Pair<List<T>, Integer> get(Location loc) {
        TreeLocatorNode<T> q = root;
        Pair<Boolean, Integer> pair = find(loc);
        if (pair.first) {
            List<T> list = (List<T>) current.data;
            current = q;
            return new Pair(list, pair.second);
        }
        current = q;
        return new Pair(null, pair.second);
    }

    @Override
    public Pair<Boolean, Integer> remove(T e, Location loc) {
        TreeLocatorNode<T> q = root;
        Pair<Boolean, Integer> pair = find(loc);
        if (pair.first) {
            if (contains((T) e)) {
                current.data.remove();
                current = q;
                return new Pair(true, pair.second);
            } else {
                current = q;
                return new Pair(false, pair.second);
            }
        }
        current = q;
        return new Pair(false, pair.second);
    }

    private boolean contains(Object e) {
        if (current.data.empty())
            return false;
        current.data.findFirst();
        while (!current.data.last()) {
            if (current.data.retrieve().equals(e))
                return true;
            current.data.findNext();
        }
        if (current.data.retrieve().equals(e))
            return true;
        return false;
    }

    @Override
    public List<Pair<Location, List<T>>> getAll() {
        TreeLocatorNode<T> p = root;
        List<Pair<Location, List<T>>> list = new LinkedList<>();
        recursive(p, list);
        return list;
    }

    private void recursive(TreeLocatorNode<T> p, List<Pair<Location, List<T>>> keys) {
        if (p == null) {
            return;
        } else {
            recursive(p.c1, keys);
            recursive(p.c2, keys);
            recursive(p.c3, keys);
            recursive(p.c4, keys);
            keys.insert(new Pair(p.location, p.data));
        }
    }

    @Override
    public Pair<List<Pair<Location, List<T>>>, Integer> inRange(Location lowerLeft, Location upperRight) {
        TreeLocatorNode<T> p = root;
        List<Pair<Location, List<T>>> list = new LinkedList<>();
        int count = getInRange(0, list, p, lowerLeft, upperRight);
        return new Pair<List<Pair<Location, List<T>>>, Integer>(list, count);
    }

    private int getInRange(int count, List<Pair<Location, List<T>>> list, TreeLocatorNode node, Location lowerLeft, Location upperRight) {
        if (node == null) {
            return count;
        }
        count++;
        if (isvalid(lowerLeft, upperRight, node.location)) {
            list.insert(new Pair(node.location, node.data));
        }
        if (isvalidCh1(lowerLeft, node.location)) {
            count += getInRange(0, list, node.c1, lowerLeft, upperRight);
        }
        if (isvalidCh2(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c2, lowerLeft, upperRight);
        }
        if (isvalidCh3(upperRight, node.location)) {
            count += getInRange(0, list, node.c3, lowerLeft, upperRight);
        }
        if (isvalidCh4(lowerLeft, upperRight, node.location)) {
            count += getInRange(0, list, node.c4, lowerLeft, upperRight);
        }
        return count;
    }


    private boolean isvalid(Location lowerLeft, Location upperRight, Location current) {
        return (lowerLeft.x <= current.x && lowerLeft.y <= current.y) && (upperRight.x >= current.x && upperRight.y >= current.y);
    }

    private boolean isvalidCh1(Location lowerLeft, Location current) {
        return lowerLeft.x < current.x && lowerLeft.y <= current.y;
    }

    private boolean isvalidCh2(Location lowerLeft, Location upperRight, Location current) {
        return lowerLeft.x <= current.x && upperRight.y > current.y;
    }

    private boolean isvalidCh3(Location upperRight, Location current) {
        return upperRight.x > current.x && upperRight.y >= current.y;
    }

    private boolean isvalidCh4(Location lowerLeft, Location upperRight, Location current) {
        return upperRight.x >= current.x && lowerLeft.y < current.y;
    }
}
