import java.util.ArrayList;

public class BST<K extends Comparable<K>, T> implements Map<K, T> {


    BSTNode<K, T> root, current;
    @Override
    public boolean empty() {
        return root == null;
    }

    @Override
    public boolean full() {
        return false;
    }

    @Override
    public T retrieve() {
        return (T) current.data;
    }

    @Override
    public void update(T e) {
        current.data = e;
    }

    @Override
    public Pair<Boolean, Integer> find(K key) {
        BSTNode<K, T> p = root, q = root;
        int count = 0;
        if (empty()) {
            return new Pair(false, count);
        }
        while (p != null) {
            count++;
            q = p;
            int r = key.compareTo(p.key);
            if (r == 0) {
                current = p;
                return new Pair(true, count);
            } else if (r < 0) {
                p = p.left;
            } else {
                p = p.right;
            }
        }
        current = q;
        return new Pair(false, count);
    }

    @Override
    public Pair<Boolean, Integer> insert(K key, T data) {

        if (empty()) {
            current = root = new BSTNode<K, T>(key, data);
            return new Pair(true, 0);
        }

        BSTNode<K, T> p, q = current;
        Pair<Boolean, Integer> pair = find(key);
        if (pair.first) {
            current = q;
            return new Pair(false, pair.second);
        }
        p = new BSTNode<>(key, data);

        if (key.compareTo(current.key) < 0) {
            current.left = p;
        } else {
            current.right = p;
        }
        current = p;
        return new Pair(true, pair.second);
    }

    @Override
    public Pair<Boolean, Integer> remove(K key) {
        // TODO Auto-generated method stub
        BSTNode<K, T> q = null;
        BSTNode<K, T> p = root;
        int counter = 0;
        K k1 = key;
        while (p != null) {
            counter++;
            int r = key.compareTo(p.key);
            if (r < 0) {
                if (p.left != null) {
                    p = p.left;
                }
            } else if (r > 0) {
                if (p.right != null) {
                    p = p.right;
                }
            } else {
                if (p.left != null && p.right != null) {
                    BSTNode<K, T> min = p.right;
                    q = p;
                    while (min.left != null) {
                        q = min;
                        min = min.left;
                    }
                    p.key = min.key;
                    p.data = min.data;
                    k1 = min.key;
                    p = min;
                } else if (p.left != null) {
                    p = p.left;
                } else {
                    p = p.right;
                }

                if (q == null) {
                    root = p;
                } else {

                }
                return new Pair(new Boolean(true), counter);
            }
        }
        return new Pair(new Boolean(false), counter);

    }

    @Override
    public List<K> getAll() {
        // TODO Auto-generated method stub
        List<K> result = new LinkedList<K>();
        dfs(root, result);
        return result;
    }

    private void dfs(BSTNode<K, T> node, List<K> result) {
        if (node == null) {
            return;
        }
        dfs(node.left, result);
        result.insert(node.key);
        dfs(node.right, result);
    }


    public void printBFS() {
        BSTNode<K, T> temp = root;
        if (temp == null) {
            System.out.println("Empty BST");
            return;
        }
        ArrayList<BSTNode> nodes = new ArrayList<>();
        nodes.add(temp);
        while (!nodes.isEmpty()) {
            BSTNode node = nodes.get(0);
            System.out.print(node.key + " ");
            nodes.remove(0);
//			System.out.print();
            if (node.right != null)
                nodes.add(node.right);
            if (node.left != null)
                nodes.add(node.left);

        }

    }
}
